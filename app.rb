$: << File.expand_path('lib', File.dirname(__FILE__))

require 'goliath'
require 'aws'
require 's3uploader'

class ExceptionHandler
  include Goliath::Rack::AsyncMiddleware
  include Goliath::Rack::Validator

  def call(env)
    begin
      super
    rescue Goliath::Validation::Error => e
      validation_error(e.status_code, e.message)
    end
  end
end

#
# To run this, start the 'app.rb' server on port 9000:
#
#   AWS_S3_BUCKET=YOUR_BUCKET_NAME bundle exec ruby -sv -p 9000
#
# Now curl the s3uploader:
#
#   curl --upload-file UPLOAD_FILE 127.0.0.1:9000/upload
#
class App < Goliath::API
  use Goliath::Rack::Heartbeat # respond to /status with 200, OK (monitoring, etc)
  use ExceptionHandler         # turn raised errors into HTTP responses

  get "/status"
  put "/upload", S3Uploader::Uploader

  def on_headers env, headers
    unless ROUTES.include?(env[PATH_INFO])
      raise Goliath::Validation::Error.new(404, "Not Found")
    end
  end

  ROUTES = maps.map{|m| m[0]}
end
