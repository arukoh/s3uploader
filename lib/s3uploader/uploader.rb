require 'uuidtools'

module S3Uploader
  class Uploader < Goliath::API
    MAX_BODY          = 1024 * (80 + 32)
    BODY_TMPFILE      = 's3uploader-body'.freeze
    UPLOAD_HEADERS    = 'upload_headers'.freeze
    UPLOAD_DATA       = 'upload_data'.freeze
    UPLOAD_MULTI      = 'upload_multi'.freeze
    UPLOAD_OBJECT_KEY = 'upload_object_key'.freeze

    include S3Uploader::Proxy

    def on_headers env, headers
      env.logger.info 'received headers: ' + headers.inspect
      env[UPLOAD_HEADERS]    = headers
      env[UPLOAD_DATA]       = StringIO.new
      env[UPLOAD_OBJECT_KEY] = gen_key
    end

    def on_body env, data
      env[UPLOAD_DATA].write(data)
      move_body_to_tempfile if move_to_tempfile?
      multipart_upload if multipart?
    end

    def on_close env
      env.logger.info 'closing connection'
    end

    def response env
      case
      when env[UPLOAD_MULTI]
        env[UPLOAD_MULTI].add_part(upload_data)
      else
        upload(env[UPLOAD_OBJECT_KEY], upload_data)
      end
      [200, {}, {object_key: env[UPLOAD_OBJECT_KEY]}]
    rescue => e
      env.logger.error("#{e.class} - #{e.message}")
      [500, {}, Goliath::HTTP_STATUS_CODES[500]]
    ensure
      close
    end

    private

    def gen_key
      UUIDTools::UUID.timestamp_create.to_s
    end

    def move_to_tempfile?
      env[UPLOAD_DATA].class == StringIO && env[UPLOAD_DATA].size > MAX_BODY
    end

    def move_body_to_tempfile
      current_body = env[UPLOAD_DATA]
      current_body.rewind
      body = Tempfile.new(BODY_TMPFILE)
      body.binmode
      body << current_body.read
      env[UPLOAD_DATA] = body
    end

    def multipart?
      env[UPLOAD_DATA].class == Tempfile && env[UPLOAD_DATA].size > multipart_min_part_size
    end

    def multipart_upload
      env[UPLOAD_MULTI] = initiate_upload(env[UPLOAD_OBJECT_KEY], {}) unless env[UPLOAD_MULTI]
      env[UPLOAD_MULTI].add_part(upload_data)
      env[UPLOAD_DATA].delete
      env[UPLOAD_DATA] = StringIO.new
    end

    def upload_data
      env[UPLOAD_DATA].rewind
      case env[UPLOAD_DATA].class
      when Tempfile
        { :file => env[UPLOAD_DATA] }
      else
        env[UPLOAD_DATA].read
      end
    end

    def close
      env[UPLOAD_MULTI].close if env[UPLOAD_MULTI]
      env[UPLOAD_DATA].close
    end

  end
end
