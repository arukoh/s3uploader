module S3Uploader
  module Proxy

    def upload key, data, options = {}
      object(key).write(data, default_options.merge(options))
    end

    def multipart_min_part_size
      AWS.config.s3_multipart_min_part_size
    end

    def initiate_upload key, options = {}
      object(key).multipart_upload default_options.merge(options)
    end
    module_function :initiate_upload

    def upload_part key, data, upload_id, part_number
      upload = object(key).multipart_uploads[upload_id]
      upload.add_part(data, :part_number => part_number) 
    end
    module_function :upload_part

    def complete_upload key, upload_id
      upload = object(key).multipart_uploads[upload_id]
      upload.complete(:remote_parts)
    end
    module_function :complete_upload

    private

    def default_options
      { :acl => :private, :storage_class => :reduced_redundancy }
    end

    def object key
      bucket.objects[key]
    end
  end
end
