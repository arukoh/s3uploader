require 's3uploader/proxy'
require 's3uploader/uploader'

module S3Uploader
  autoload :Proxy,    'proxy'
  autoload :Uploader, 'uploader'
end
