require 'logger'

BUCKET_NAME = ENV['AWS_S3_BUCKET']

def log_formatter
  uri_pattern = []
  uri_pattern << ':http_request_host'
  uri_pattern << '::'
  uri_pattern << ':http_request_port'
  uri_pattern << ':'
  uri_pattern << ':http_request_uri'

  pattern = []
  pattern << "[AWS"
  pattern << ":service"
  pattern << ":http_response_status"
  pattern << ":duration"
  pattern << ":retry_count]"
  pattern << ":http_request_method"
  pattern << "#{uri_pattern.join}"
  pattern << ":error_class"
  pattern << ":error_message"
  AWS::Core::LogFormatter.new(pattern.join(' ') + "\n")
end

config['bucket'] = AWS::S3.new({
  :s3_endpoint                => 's3-ap-northeast-1.amazonaws.com',
#  :s3_multipart_threshold     => 16 * 1024 * 1024, # 16MB
#  :s3_multipart_min_part_size => 5 * 1024 * 1024,  # 5MB
#  :s3_multipart_max_parts     => 1000,
#  :s3_server_side_encryption  => :aes256,
  :logger                     => Logger.new($stdout),
#  :log_level                  => :debug,
#  :log_formatter              => AWS::Core::LogFormatter.debug,
  :log_formatter              => log_formatter,
  :proxy_uri                  => [ENV['HTTPS_PROXY'],ENV['https_proxy']].compact.uniq[0]
}).buckets[BUCKET_NAME]
